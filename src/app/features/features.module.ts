import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductsComponent } from './products/products.component';
import { CartComponent } from './cart/cart.component';
import { ProductComponent } from './products/product/product.component';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [ProductsComponent, CartComponent, ProductComponent],
  imports: [CommonModule, SharedModule],
  exports: [CartComponent, ProductsComponent, ProductComponent],
})
export class FeaturesModule {}
